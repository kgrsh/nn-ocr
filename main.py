
# coding: utf-8

# In[63]:

print("Importing dependencies")
import numpy as np
import cv2
#import skimage

#get_ipython().magic('matplotlib notebook')
#from IPython.display import display
from scipy.misc import toimage
#from matplotlib import pyplot as plt
from operator import itemgetter
from typing import List,Tuple
from time import time
from PIL import Image,ImageOps
from keras.models import load_model
from io import StringIO
from sys import argv
infinity=-np.iinfo(np.int64).max
glyph_names=['period', 'left_paren', 'question', 'comma', 'dash', 'colon', 'right_paren',
       'semicolon', 'exclamation', 'left_quote', 'left_quote_1', 'right_quote', 'right_quote_1',
       '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 
       'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т',
       'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ы_1', 'Ь', 'Э', 'Ю', 'Я', 
       'а_', 'б_', 'е_', 'ф_']
glyph_symbols=['.', '(', '?', ',', '-', ':', ')', ';', '!', '«', '<', '»', '>',
       '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 
       'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т',
       'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', '|', 'Ь', 'Э', 'Ю', 'Я', 
       'а', 'б', 'е', 'ф']


# In[43]:

#for i in zip(range(1000),glyph_names,glyph_symbols):
#    print(i)


# In[3]:

nn_model=load_model(r'model1.h5')


# In[4]:

def unique_rows(arr:np.ndarray):
    np.fromiter((hash(str(row)) for row in testarr),np.intp)


# In[5]:

def rotate_img(img: np.ndarray,angle: float) -> np.ndarray:
    rows,cols=img.shape[0:2]
    r_mat=cv2.getRotationMatrix2D((rows/2,cols/2),angle,1)
    return cv2.warpAffine(img,r_mat,(cols,rows))


# In[6]:

def binarize(img: np.ndarray) -> np.ndarray:
    average=np.average(img)*2
    return (img>average).astype(np.uint8)*255


# In[7]:

def convolve_1d(arr: np.ndarray,kernel=np.full((3,),1,dtype=np.int)) -> np.ndarray:
    size=len(kernel)
    stack=np.stack([arr[i:-(size-1-i)] for i in range(size-1)]+[arr[size-1:]])
    pad_width=(int(np.round((size-1)/2)),int(np.floor((size-1)/2)))
    return np.pad(np.max(stack,axis=0),pad_width,'edge')


# In[8]:

def convolve_1d_min(arr: np.ndarray,kernel=np.full((3,),1,dtype=np.int)) -> np.ndarray:
    size=len(kernel)
    stack=np.stack([arr[i:-(size-1-i)] for i in range(size-1)]+[arr[size-1:]])
    pad_width=(int(np.round((size-1)/2)),int(np.floor((size-1)/2)))
    return np.pad(np.min(stack,axis=0),pad_width,'edge')


# In[9]:

def prepare(filename:str) -> np.ndarray:
    print("preparing",filename)
    img=255-cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
    average=np.average(img)*2
    foreground_mask=(img>average).astype(np.uint8)*255
    kernel=cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
    opened=cv2.morphologyEx(foreground_mask,cv2.MORPH_OPEN,kernel)
    rect=cv2.minAreaRect(np.stack(np.nonzero(opened),axis=-1))
    skew_angle=min(90+rect[2],abs(rect[2]))
    angles=np.linspace(skew_angle+0.5,skew_angle-0.5,11)
    img_list=[rotate_img(foreground_mask,-angle) for angle in angles]
    return img_list


# In[10]:

def split_lines(img: np.ndarray) -> List[np.ndarray]:
    data=np.max(img,axis=-1)
    data_c=convolve_1d_min(data)
    nonmax=data_c<np.max(data_c)
    return np.nonzero(np.logical_xor(nonmax[1:],nonmax[:-1]))[0].reshape(-1,2)
    #print(percs.shape,percs_c.shape)
    #plt.plot(percs,label='before')
    #plt.plot(percs_c,label='after')
    #plt.plot(np.full_like(percs,np.average(percs_c)),label='av')
    #plt.legend()


# In[11]:

def join_lines(boundaries: List[np.ndarray]): #-> [np.ndarray]:
    widths=np.array([line[1]-line[0] for line in boundaries])
    threshold=np.median(widths)*0.3
    zero_array=np.array([0,0])
    distance_previous=[infinity]+[boundaries[i][0]-boundaries[i-1][1] for i in range(1,len(boundaries))]
    distance_next=[boundaries[i+1][0]-boundaries[i][1] for i in range(len(boundaries)-1)]+[infinity]
    distances=np.stack((np.array(distance_previous),np.array(distance_next)),axis=-1)
    #print(boundaries.shape,widths.shape,distances.shape)
    for i in range(len(boundaries)):
        if widths[i] <= threshold:
            if distances[i][0] < distances[i][1]:
                boundaries[i-1][1]=boundaries[i][1]
                boundaries[i]=zero_array
            else:
                boundaries[i+1][0]=boundaries[i][0]
                boundaries[i]=zero_array
    return boundaries[np.all(boundaries!=zero_array,axis=1)]


# In[12]:

def get_hor_boundaries(arr: np.ndarray) -> Tuple[int]:
    nonzero_lines=np.nonzero(np.transpose(arr))[0]
    left=np.min(nonzero_lines)
    right=np.max(nonzero_lines)
    return (left,right)


# In[13]:

def get_ver_boundaries(arr: np.ndarray) -> Tuple[int]:
    nonzero_lines=np.nonzero(arr)[0]
    top=np.min(nonzero_lines)
    bottom=np.max(nonzero_lines)
    return (top,bottom)


# In[14]:

def merge_letter_fragments_v(letter_arr: np.ndarray) -> np.ndarray:
    boundaries=np.array([get_hor_boundaries(letter) for letter in letter_arr])
    #v_boundaries=np.array([get_ver_boundaries(letter) for letter in letter_arr])
    widths=boundaries[:,1]-boundaries[:,0]
    #heights=v_boundaries[:,1]-v_boundaries[:,0]
    median_width=np.median(widths)
    #joining_threshold = np.median(widths)*0.75
    #joining_threshold_v = np.median(heights)*0.75
    #print(median_width,'h',joining_threshold,'v',joining_threshold_v,)
    offset=median_width*0.25
    offset=np.array((-offset,offset))
    extended_boundaries=boundaries+offset
    for i in range(1,len(boundaries)):
        if (boundaries[i-1][0] >  extended_boundaries[i][0]) and            (boundaries[i-1][1] <  extended_boundaries[i][1]) or            (boundaries[i][0] >  extended_boundaries[i-1][0]) and            (boundaries[i][1] <  extended_boundaries[i-1][1]) or            (boundaries[i-1][1] ==  boundaries[i][0]):
           #widths[i-1] < joining_threshold and \
           #widths[i] < joining_threshold and \
           #heights[i-1] > joining_threshold_v and \
           #heights[i] > joining_threshold_v or \
           
            letter_arr[i]+=letter_arr[i-1]
            letter_arr[i-1]=np.zeros_like(letter_arr[i-1])
            #boundaries[i]=
    return letter_arr[np.sum(letter_arr,axis=(-1,-2))>int(len(letter_arr[0])/2)]


# In[15]:

def get_statistics(letter_arr: np.ndarray) -> np.ndarray:
    boundaries=np.array([get_hor_boundaries(letter) for letter in letter_arr])
    v_boundaries=np.array([get_ver_boundaries(letter) for letter in letter_arr])
    widths=boundaries[:,1]-boundaries[:,0]
    heights=v_boundaries[:,1]-v_boundaries[:,0]
    tops=np.transpose(v_boundaries)[0]
    bottoms=np.transpose(v_boundaries)[1]
    median_top=np.median(tops)
    median_bottom=np.median(bottoms)
    median_height=np.median(heights)
    top_overhang=(tops-median_top)/median_height
    bottom_overhang=(bottoms-median_bottom)/median_height
    proportions=heights/widths
    return np.transpose(np.stack((proportions,top_overhang,bottom_overhang,widths)))


# In[16]:

def get_widths(letter_arr: np.ndarray) -> np.ndarray:
    boundaries=np.array([get_hor_boundaries(letter) for letter in letter_arr])
    return boundaries[:,1]-boundaries[:,0]


# In[17]:

def resize_letter(mask: np.ndarray) -> None:
    boundaries=[get_ver_boundaries(mask),get_hor_boundaries(mask)]
    boundaries[0]=(boundaries[0][0],boundaries[0][1]+1)
    fragment=mask[slice(*boundaries[0]),slice(*boundaries[1])]
    return (cv2.resize(fragment.astype('uint8'),(32,32),interpolation=cv2.INTER_CUBIC))


# In[18]:

def merge_letter_fragments_h(letter_arr: np.ndarray) -> np.ndarray:
    boundaries=np.array([get_hor_boundaries(letter) for letter in letter_arr])
    v_boundaries=np.array([get_ver_boundaries(letter) for letter in letter_arr])
    widths=boundaries[:,1]-boundaries[:,0]
    distances=np.pad(boundaries[1:,0]-boundaries[:-1,1],(1,1),'constant',constant_values=np.iinfo('int32').max)
    heights=v_boundaries[:,1]-v_boundaries[:,0]
    joining_threshold = np.median(widths)*0.65
    joining_threshold_v = np.median(heights)*0.6
    joining_threshold_v_m = np.median(heights)*1.1
    to_join=[]
    for i in range(0,len(boundaries)):
#        if  widths[i-1] < joining_threshold and \
#            heights[i-1] > joining_threshold_v and \
        if  widths[i] < joining_threshold and             heights[i] > joining_threshold_v and             heights[i] < joining_threshold_v_m:
            if distances[i+1]<distances[i]:
                to_join.append(i+1)
            else:
                to_join.append(i)
#    for i in range(1,len(boundaries)):
#        if (boundaries[i-1][1] <=  boundaries[i][0]):
#            to_join.append(i)
    for i in range(1,len(to_join)):
        if to_join[i]==to_join[i-1]+1:
            to_join[i]=-1
    to_join=[i for i in to_join if i>0]
    if False:
        print(to_join)
        print(distances)
        print(distances[1:]<distances[:-1])
        print(np.logical_and(heights > joining_threshold_v, widths < joining_threshold))
    for join_ind in to_join:
        letter_arr[join_ind-1]+=letter_arr[join_ind]
        letter_arr[join_ind]=np.zeros_like(letter_arr[join_ind-1])
    return letter_arr[np.sum(letter_arr,axis=(-1,-2))>int(len(letter_arr[0])/2)]


# In[19]:

def split_into(letter_img: np.ndarray, count:int) -> np.ndarray:
    boundaries=get_hor_boundaries(letter_img)
    rgb=np.stack((letter_img,letter_img,letter_img),axis=-1).astype('uint8')
    markers=letter_img.astype('int32')
    for marker_value,marked_col in enumerate(np.linspace(*boundaries,count*2+1)[1::2],start=1):
        markers[:,int(marked_col)]*=marker_value+1
    markers-=1
    cv2.watershed(rgb,markers)
    return [np.where(markers==i,letter_img,np.zeros_like(letter_img)) for i in range(1,count+1)]


# In[39]:

def split_letter_blobs(letter_arr: np.ndarray) -> np.ndarray:
    boundaries=np.array([get_hor_boundaries(letter) for letter in letter_arr])
    v_boundaries=np.array([get_ver_boundaries(letter) for letter in letter_arr])
    widths=boundaries[:,1]-boundaries[:,0]
    heights=v_boundaries[:,1]-v_boundaries[:,0]
    median_width=np.median(widths)
    median_height=np.median(heights)
    median_top=np.median(v_boundaries[:,0])
    letter_arr_new=[]
    for i in range(0,len(boundaries)):
        if widths[i]<1.8*median_width:
            letter_arr_new.append(letter_arr[i])
        else:
            try:
                split_letter_imgs=split_into(letter_arr[i],int(np.round(widths[i]/(median_width))))
                v_boundaries_new=np.array([get_ver_boundaries(letter) for letter in split_letter_imgs])
                top_overhang_new=(median_top-v_boundaries_new[:,0])/median_height
                if np.any(top_overhang_new<0.25):
                    for img in split_letter_imgs:
                        letter_arr_new.append(img)
                else:
                    letter_arr_new.append(letter_arr[i])
            except:
                letter_arr_new.append(letter_arr[i])
    return np.array(letter_arr_new)


# In[21]:

def split_letters(img: np.ndarray) -> np.ndarray:
    #filtered=np.maximum(img[:,:-1],img[:,1:])
    #filtered=np.minimum(filtered[:,:-1],filtered[:,1:])
    #filtered=(img>np.average(img)).astype('uint8')
    filtered=img
    no_markers,labeled=cv2.connectedComponents(np.transpose(filtered),connectivity=4)
    letters=np.array([np.transpose(labeled==i) for i in range(1,no_markers)])
    sum_threshold=int(len(letters[0])/2)
    unmerged=letters[np.sum(letters,axis=(-1,-2))>sum_threshold]
    return [i for i in split_letter_blobs(merge_letter_fragments_h(merge_letter_fragments_v(unmerged)))]


# In[22]:

def split_letters_no_merge(img: np.ndarray) -> np.ndarray:
    #filtered=np.maximum(img[:,:-1],img[:,1:])
    #filtered=np.minimum(filtered[:,:-1],filtered[:,1:])
    #filtered=(img>np.average(img)).astype('uint8')
    filtered=img
    no_markers,labeled=cv2.connectedComponents(np.transpose(filtered),connectivity=4)
    letters=np.array([np.transpose(labeled==i) for i in range(1,no_markers)])
    sum_threshold=int(len(letters[0])/2)
    unmerged=letters[np.sum(letters,axis=(-1,-2))>sum_threshold]
    return unmerged


# In[60]:

def correct_prediction(prediction,top_overhang,bottom_overhang,proportion):
    if proportion<0.5 and -0.3>top_overhang and -0.3>bottom_overhang:
        return '-'
    if proportion>2.5 and 0.2>top_overhang>-0.2 and 0.2>bottom_overhang>-0.2:
        return '|'
    if top_overhang<-0.6:
        if proportion<1.5:
            return '.'
        else:
            return ','
    if prediction==32:
        if top_overhang>0.8:
            return glyph_symbols[prediction].upper()
        else:
            return glyph_symbols[prediction].lower()
    if prediction in (1,40):
        if proportion>1.5:
            return '('
        else:
            if top_overhang>0.25:
                return 'C'
            else:
                return 'c'
    if prediction in (22,37):
        if proportion>1.25:
            return '0'
        else:
            if top_overhang>0.25:
                return 'О'
            else:
                return 'о'
    if prediction in range(23,56):
        if top_overhang>0.25:
            return glyph_symbols[prediction].upper()
        else:
            return glyph_symbols[prediction].lower()
    else:
        return glyph_symbols[prediction]


# In[62]:

def recognize(filename):
    img_list=prepare(filename)
    lines_list=[split_lines(img) for img in img_list]
    lengths=[len(lines) for lines in lines_list]
    img=img_list[np.argmax(lengths)]
    page_split=join_lines(lines_list[np.argmax(lengths)])
    print(len(page_split),'lines')
    line_imgs=[img[slice(*page_split[i])] for i in range(len(page_split))]

    letter_masks_selected=[]
    letter_masks_selected_unsplit=[]
    bin_line_imgs_selected=[]
    line_ind_=7
    #for line_img in line_imgs[line_ind_:line_ind_+1]:
    for line_ind in range(len(line_imgs)):

        #bin_line_imgs=[(line_img>threshold) for threshold in np.arange(32,241,32)]
        line_img=line_imgs[line_ind]
        average_brightness=np.average(line_img)
        bin_line_imgs=[(line_img>threshold) for threshold in                        np.arange(average_brightness/2,255,average_brightness/2).astype('int')[:12]]
        letter_masks_list=[split_letters_no_merge(bin_line_img.astype('uint8')) for bin_line_img in bin_line_imgs]
        widths_list=[get_widths(letter_masks) for letter_masks in letter_masks_list]
        binc_list=[np.bincount(widths) for widths in widths_list] 
        median_width_list=[np.median(widths) for widths in widths_list] 
        width_thresholds=[(int(np.floor(median_width*0.8)),int(np.ceil(median_width*1.2))) for median_width in median_width_list]
        sums=[np.sum(np.concatenate((binc_list[i][:width_thresholds[i][0]],
                                     binc_list[i][width_thresholds[i][1]:]))) for i in range(len(binc_list))]
        selected_index=np.median(np.nonzero(sums==np.min(sums))).astype('int64')
        letter_masks_selected.append(split_letter_blobs(
            merge_letter_fragments_h(merge_letter_fragments_v(letter_masks_list[selected_index]))))
        bin_line_imgs_selected.append(bin_line_imgs[selected_index])
        print('splitting line {}/{}'.format(line_ind+1,len(line_imgs)),end='\r')

    letter_masks_resized=[np.array([resize_letter(letter) for letter in line]) for line in letter_masks_selected]
    hor_boundaries=[np.array([get_hor_boundaries(letter) for letter in line]) for line in letter_masks_selected]
    ver_boundaries=[np.array([get_ver_boundaries(letter) for letter in line]) for line in letter_masks_selected]
    widths=[line[:,1]-line[:,0] for line in hor_boundaries]
    heights=[line[:,1]-line[:,0] for line in ver_boundaries]
    proportions=[height_line/width_line for height_line,width_line in zip(heights,widths)]
    median_heights=np.array([np.median(height_line) for height_line in heights])
    median_widths=np.array([np.median(width_line) for width_line in widths])
    median_tops=[np.median(line[:,0]) for line in ver_boundaries]
    median_bottoms=[np.median(line[:,1]) for line in ver_boundaries]
    top_overhangs=[(median_top - line[:,0])/median_height 
                   for median_top,median_height,line in zip(median_tops,median_heights,ver_boundaries) ]
    bottom_overhangs=[(line[:,1] - median_bottom)/median_height 
                   for median_bottom,median_height,line in zip(median_bottoms,median_heights,ver_boundaries) ]
    distances=[line[1:,0]-line[:-1,1] for line in hor_boundaries]
    dist_binc=np.bincount(np.abs(np.concatenate(distances))).astype('float')
    dist_binc+=1
    space_threshold=np.argmax(dist_binc[:-1]/dist_binc[1:])
    space_after=[np.pad(line>(space_threshold),(0,1),'constant') for line,median_width in zip(distances,median_widths)]
    predictions=[np.argmax(nn_model.predict((line_imgs).reshape(-1,32,32,1)),axis=-1) for line_imgs in letter_masks_resized]

    output_io=StringIO()
    for predictions_line,space_after_line,top_overhang_line,bottom_overhang_line,proportion_line     in zip(predictions,space_after,top_overhangs,bottom_overhangs,proportions):
        for prediction,space_after_letter,top_overhang,bottom_overhang,proportion         in zip(predictions_line,space_after_line,top_overhang_line,bottom_overhang_line,proportion_line):
            output_io.write(correct_prediction(prediction,top_overhang,bottom_overhang,proportion))
            output_io.write(' ') if space_after_letter else None
        output_io.write('\n')
    return output_io.getvalue()


# In[61]:




# In[66]:

if __name__=="__main__":
    for filename in argv[1:]:
        for line in recognize(filename).splitlines():
            print(line)


# In[ ]:



