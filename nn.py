import numpy as np
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D,MaxPool2D
from keras.layers.core import Reshape,Dense,Flatten,Dropout
from keras.losses import categorical_crossentropy
from keras.optimizers import SGD,Adadelta
from keras.utils.np_utils import to_categorical

import atexit
@atexit.register
def save():
    model.save('model.h5')

targets=np.load(r'D:\occr2\data-targets.npy')
features=np.load(r'D:\occr2\data-features-dense.npy').reshape(-1,32,32,1)
targets_categorical = to_categorical(targets)

model=Sequential()
model.add(Conv2D(12,(5,5),activation='relu',input_shape=(32,32,1)))
model.add(Conv2D(24,(5,5),activation='relu'))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(units=64,activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(units=60,activation='softmax'))
model.compile(optimizer='Adadelta', loss='categorical_crossentropy', metrics=['accuracy'])
hist=model.fit(features,targets_categorical,epochs=32,validation_split=0.2)
